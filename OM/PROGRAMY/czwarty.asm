;   PROGRAM  "pierwszy.asm"

dane SEGMENT 	;segment danych

we 	dw 65, 23h, 65, 67, 65, 67, 65, 67, 65, 67, 65, 67
dw 65, 67, 65, 67, 65, 67, 65, 67, 65, 67, 65, 67
dw 65, 67, 65, 67, 65, 67, 65, 67
koniec_we dw ?

wy 	dw 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
dw 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
koniec_wy dw ?
Buf_2 db 128 dup(0)
;shift zamiast dzielenia

srednia dw 1
koniec_sr dw ?

dane ENDS




rozkazy SEGMENT 'CODE' use16 	;segment zawierający rozkazy programu
		ASSUME cs:rozkazy, ds:dane
wystartuj:
		mov ax, SEG dane
		mov ds, ax
		
		mov dx, OFFSET srednia 
		push dx
		
		mov dx, OFFSET we 
		push dx
		
		mov dx, 32
		push dx
		
		call liczSr
			
		pop dx
		call wysZnak
		
		
		pop dx ;zrzucenie we
		pop dx ;zrzucenie srednia
		
		mov bx, offset we
		call piszLiczbe
		
		
		call wysZnak
		
		mov dx, offset srednia
		push dx
		
		mov dx, offset we
		push dx
		
		mov dx, offset wy
		push dx
		
		call normalizacja
		
		
		mov bx, offset wy
		call piszLiczbe


		
		
		mov al, 0 	;kod powrotu programu (przekazywany przez
				;rejestr AL) stanowi syntetyczny opis programu
				;przekazywany do systemu operacyjnego
				;(zazwyczaj kod 0 oznacza, że program został
				;wykonany poprawnie)
		
				
		call zakoncz
		
		
normalizacja PROC near
		push  bp
		mov  bp, sp   ;po wykonaniu tej instrukcji rejestr BP 
		;będzie wskazywał wierzchołek stosu 
		mov  si, word PTR[bp]+4  ;wskaznik do wyjscia ciag
		mov bx, word PTR[bp]+6 ; wskaznik do wejscia ciag
		mov cx, 32
mnozenie80:
		mov ax, 80 ; iloraz
		push si ; chwilowo zrzucamy wskaznik do ciagu wyjsciowego na stos
		mov si, [bx]; do SI wpsiujemy konkretna wartosc
		mul si; wykonujemy wynik mnozenia, (DX AX) = SI*AX, DX nas
		;nie obchodzi bo 80*127 nie przekroczy wartosci max
		pop si; na powrot ustawiamy wskaznik do ciagu wyjsiciowego
		push bx ; na chwile zrzucamy wskaznik do ciagu wej na stos
		mov bx, word PTR[bp]+8 ; wskaznik do sredniej
		sub ax, [bx] ; odejmujemy od wyniku
		pop bx; 
		mov [si], ax
		
		inc bx ; podwojnie bo double word
		inc bx
		inc si
		inc si
		dec cx
		cmp cx, 0
		jne mnozenie80
		

		pop  bp    ;odtworzenie rejestru EBP 
	
	ret
normalizacja ENDP
	
wysZnak  PROC  near 
	mov  ah, 2   ;przechowanie AL 
	int 21h
	
	mov dl, 13
	mov  ah, 2   ;przechowanie AL 
	int 21h
	
	mov dl, 10
	mov  ah, 2   ;przechowanie AL 
	int 21h
	ret  
wysZnak  ENDP 


piszLiczbe PROC near
		mov dx, 32
petlaPoLiczach:	
		push dx
		push bx
		mov ax, [bx]
		mov  cx, 5  ;liczba obiegów pętli Tak dlugi bedzie wyraz
		mov  bx, 10  ; dzielnik 
		mov  si, 4  ;indeks początkowy w tablicy tbl_cyfr wpisujemy od konca
petlaPoCyfrach:  mov  dx, 0  ;zerowanie starszej części dzielnej 
		div  bx   ;dzielenie przez 10; iloraz w AX, reszta w DX 
		add  dx, 30H  ;zamiana reszty na kod ASCII 
		mov  buf_2[si], dl ;odesłanie kodu ASCII kolejnej cyfry do tablicy 
		dec  si   ;zmniejszenie indeksu w rejestrze SI o 1 
		loop  petlaPoCyfrach   ;sterowanie pętlą
		
		mov dl, ' ' 	;wpisanie do rejestru DL kodu ASCII
		mov ah, 2
		int 21H 
		
		
		mov cx, 5
		mov dx, offset buf_2
		call wysTekst
		pop bx
		pop dx
		inc bx
		inc bx ; dwa razy inc bo double word
		dec dx
		cmp dx,0
		jne petlaPoLiczach
		
		ret
piszLiczbe ENDP

wysTekst PROC  near 
		mov bx, dx
	ptl:
		mov dl, [bx] 	;wpisanie do rejestru DL kodu ASCII
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		inc bx 		;inkrementacja adresu kolejnego znaku
	
loop ptl 			;sterowanie pętlą

	ret

wysTekst  ENDP

	
liczSr PROC  near
		
		push  bp
		mov  bp, sp   ;po wykonaniu tej instrukcji rejestr BP 
		;będzie wskazywał wierzchołek stosu 
		mov  cx, word PTR[bp]+4  ;ładowanie ilosci znakow, dlugosci petli
		mov bx, word PTR[bp]+6 ; ładowanie wskaznika do tablicy we
		mov ax, 0
sumowanie:
		add ax, [bx]
		inc bx
		inc bx
		loop sumowanie
		
		shr ax, 1
		shr ax, 1
		shr ax, 1
		shr ax, 1
		shr ax, 1
		
		mov si, word PTR[bp]+8 ; ładowanie wskaznika do sredniej
		mov [si], ax
		
		pop  bp    ;odtworzenie rejestru EBP 
		pop dx ; zrzucam wartosc powrotu
		push ax; na stos wrzucam wynik srednie
	
		push dx; potem wrzucam adres powrotu
		ret    
liczSr  ENDP

zakoncz PROC  near 				
				
		mov ah, 4CH 	;zakończenie programu – przekazanie sterowania
				;do systemu, za pomocą funkcji 4CH DOS
		int 21H
		
		ret
zakoncz  ENDP

wyswietlZnak PROC  near
		mov  cx, 3  ;liczba obiegów pętli 
		; w ax musze wstawic dana ktura ma sie wyswietlic
		mov  bx, 10  ;dzielnik 
		p1:  mov  dx, 0  ;zerowanie starszej części dzielnej 
		div  bx   ;dzielenie przez 10 - iloraz w AX, reszta wDX 
		add  dx, 30  ;zamiana reszty na kod ASCII 
		push  ax   ;przechowanie ilorazu na stosie 
		mov  ah, 2 
		int  21H   ;wyświetlenie cyfry na ekranie 
		pop  ax   ;przywrócenie ilorazu w AX 
		loop  p1   ;sterowanie pętlą
		ret
wyswietlZnak  ENDP

		
rozkazy ENDS

nasz_stos SEGMENT stack 	;segment stosu
dw 128 dup (?)
nasz_stos ENDS

END wystartuj 			;wykonanie programu zacznie się od rozkazu
				;opatrzonego etykietą wystartuj