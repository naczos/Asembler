;   PROGRAM  "pierwszy.asm"

dane SEGMENT 	;segment danych
licz db 'a'
tekst 	db '243 5678 34236 92 342'
koniec_txt db ?

tekst1 	db '211111111111112111111'
koniec_txt1 db ?

dane ENDS



rozkazy SEGMENT 'CODE' use16 	;segment zawieraj�cy rozkazy programu
		ASSUME cs:rozkazy, ds:dane
wystartuj:
		mov ax, SEG dane
		mov ds, ax
		mov cx, koniec_txt-tekst
		mov bx, OFFSET tekst 	;wpisanie do rejestru BX obszaru
		mov SI, OFFSET tekst1
		mov dh, 14 ;jesli 14 to poprzednia byla spacja gdy 15 to poprzednia byla cyfra
					;zawieraj�cego wyswietlany tekst
			
		jmp ptl
			
spacja:
		mov dh, 14
		mov [SI], dl
		jmp pok

inkre:
		mov dh, 15
		cmp dl, '9'
		je ninkre
		inc dl 
		mov [SI], dl
		dec dl
		jmp pok	
		
nspac:
		cmp dh, 14
		je inkre
ninkre:
		mov [SI], dl
		jmp pok					
					
ptl:
		mov dl, [bx] 	;wpisanie do rejestru DL kodu ASCII
		
		cmp dl, 32 ;jesli pobrany znak jese spacja
		JE spacja	;jesli jest skacz do spacja
		JNE nspac
		

		

		

pok:
		mov ah, 2
		int 21H 	;wy�wietlenie znaku za pomoc� funkcji nr 2 DOS
		inc bx 		;inkrementacja adresu kolejnego znaku
		inc SI
loop ptl 			;sterowanie p�tl�

		mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wy�wietlanego znaku
		mov ah, 2
		int 21H 	;wy�wietlenie znaku za pomoc� funkcji nr 2 DOS
		mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wy�wietlanego znaku
		mov ah, 2
		int 21H 	;wy�wietlenie znaku za pomoc� funkcji nr 2 DOS


		mov cx, koniec_txt1-tekst1
		mov bx, OFFSET tekst1 
ptl1:
		mov dl, [bx] 	;wpisanie do rejestru DL kodu ASCII
		
				;kolejnego wy�wietlanego znaku
		mov ah, 2
		int 21H 	;wy�wietlenie znaku za pomoc� funkcji nr 2 DOS
		inc bx 		;inkrementacja adresu kolejnego znaku
loop ptl1 			;sterowanie p�tl�


		mov al, 0 	;kod powrotu programu (przekazywany przez
				;rejestr AL) stanowi syntetyczny opis programu
				;przekazywany do systemu operacyjnego
				;(zazwyczaj kod 0 oznacza, �e program zosta�
				;wykonany poprawnie)

		mov ah, 4CH 	;zako�czenie programu � przekazanie sterowania
				;do systemu, za pomoc� funkcji 4CH DOS
		int 21H
rozkazy ENDS

nasz_stos SEGMENT stack 	;segment stosu
dw 128 dup (?)
nasz_stos ENDS

END wystartuj 			;wykonanie programu zacznie si� od rozkazu
				;opatrzonego etykiet� wystartuj