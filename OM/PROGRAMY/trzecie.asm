;   PROGRAM  "pierwszy.asm"

dane SEGMENT 	;segment danych
licz db 'a'
tekst 	db '1344236793426215951235483218756021886201568423056842058452038453','$'
koniec_txt db ?

tekst1 	db '0000000000000000000000000000000000000000000000000000000000000000','$'
koniec_txt1 db ?



dane ENDS




rozkazy SEGMENT 'CODE' use16 	;segment zawierający rozkazy programu
		ASSUME cs:rozkazy, ds:dane
wystartuj:
		mov ax, SEG dane
		mov ds, ax
		
		
			


		
		
		mov dl, 'T'
		call wysZnak
		
		mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		
		mov cx, koniec_txt-tekst
		mov dx, OFFSET tekst 	;wpisanie do rejestru BX obszaru
		
		call wysTekst
		
		mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS

		
		mov bx, OFFSET tekst 	;wpisanie do rejestru BX obszaru
		call rosnaco
		call malejaco
		
		
		mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
		;kolejnego wyświetlanego znaku
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		
		mov cx, koniec_txt-tekst
		mov dx, OFFSET tekst 	;wpisanie do rejestru BX obszaru
		
		call wysTekst
		
		mov al, 0 	;kod powrotu programu (przekazywany przez
				;rejestr AL) stanowi syntetyczny opis programu
				;przekazywany do systemu operacyjnego
				;(zazwyczaj kod 0 oznacza, że program został
				;wykonany poprawnie)
		
				
		call zakoncz
	


zakoncz PROC  near 				
				
		mov ah, 4CH 	;zakończenie programu – przekazanie sterowania
				;do systemu, za pomocą funkcji 4CH DOS
		int 21H
		
		ret
zakoncz  ENDP

zapytanie PROC  near 
	
		
		mov ch, 0; licznik ktory to juz obrot
		inc bx
		mov ax, bx
obrot1:
		mov bx, ax 	;wpisanie do rejestru BX obszaru
		mov cl, 0; licznik ktory znak ma brac
przypis1:
		
		mov dl, [bx]
		mov dh, dl ; pierwsza zapisana do dh
		
		inc bx
		mov dl, [bx]; nowa do dl
		
		cmp dl, dh
		ja zamien1 ;jesli wieksz poprzedni od nastepnego zamien
pzamien1:
		inc cl	
		cmp cl, 15
		jne przypis1  ; jesli jeszcze nie dobilo do 16 to powtorz
		
		inc ch
		cmp ch, 20; ile razy ma obrocic
		jne obrot1
		
		
		
		jmp kros1 ; skocz do konca funkcji, nie zamieniaj
		
zamien1:
		mov [bx], dh
		dec bx
		mov [bx], dl
		inc bx
		jmp pzamien1
		
kros1:
		
	ret

zapytanie  ENDP

malejaco PROC  near 
	
		
		mov ch, 0; licznik ktory to juz obrot
		inc bx
		mov ax, bx
obrot1:
		mov bx, ax 	;wpisanie do rejestru BX obszaru
		mov cl, 0; licznik ktory znak ma brac
przypis1:
		
		mov dl, [bx]
		mov dh, dl ; pierwsza zapisana do dh
		
		inc bx
		mov dl, [bx]; nowa do dl
		
		cmp dl, dh
		ja zamien1 ;jesli wieksz poprzedni od nastepnego zamien
pzamien1:
		inc cl	
		cmp cl, 15
		jne przypis1  ; jesli jeszcze nie dobilo do 16 to powtorz
		
		inc ch
		cmp ch, 20; ile razy ma obrocic
		jne obrot1
		
		
		
		jmp kros1 ; skocz do konca funkcji, nie zamieniaj
		
zamien1:
		mov [bx], dh
		dec bx
		mov [bx], dl
		inc bx
		jmp pzamien1
		
kros1:
		
	ret

malejaco  ENDP

rosnaco PROC  near 
	
		
		mov ch, 0; licznik ktory to juz obrot
		
obrot:
		mov bx, OFFSET tekst 	;wpisanie do rejestru BX obszaru
		mov cl, 0; licznik ktory znak ma brac
przypis:
		
		mov dl, [bx]
		mov dh, dl ; pierwsza zapisana do dh
		
		inc bx
		mov dl, [bx]; nowa do dl
		
		cmp dh, dl
		ja zamien ;jesli wieksz poprzedni od nastepnego zamien
pzamien:
		inc cl	
		cmp cl, 15
		jne przypis  ; jesli jeszcze nie dobilo do 16 to powtorz
		
		inc ch
		cmp ch, 20; ile razy ma obrocic
		jne obrot
		
		
		
		jmp kros ; skocz do konca funkcji, nie zamieniaj
		
zamien:
		mov [bx], dh
		dec bx
		mov [bx], dl
		inc bx
		jmp pzamien
		
kros:
		
	ret

rosnaco  ENDP

wysTekst PROC  near 
		mov bx, dx
	ptl:
		mov dl, [bx] 	;wpisanie do rejestru DL kodu ASCII
		mov ah, 2
		int 21H 	;wyświetlenie znaku za pomocą funkcji nr 2 DOS
		inc bx 		;inkrementacja adresu kolejnego znaku
	
loop ptl 			;sterowanie pętlą

	ret

wysTekst  ENDP
		
wysZnak  PROC  near 
	mov  ah, 2   ;przechowanie AL 
	int 21h
	ret  
wysZnak  ENDP 
		
rozkazy ENDS

nasz_stos SEGMENT stack 	;segment stosu
dw 128 dup (?)
nasz_stos ENDS

END wystartuj 			;wykonanie programu zacznie się od rozkazu
				;opatrzonego etykietą wystartuj