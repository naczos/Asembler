;   PROGRAM  "pierwszy.asm"

dane SEGMENT 	;segment danych
Buf_1 db 4 dup(0)
koniec_Buf_1 db ?
LBin dw 0
Buf_2 db 128 dup(0)
dane ENDS

rozkazy SEGMENT 'CODE' use16 
		ASSUME cs:rozkazy, ds:dane
wystartuj:

	mov ax, SEG dane
	mov ds, ax
	mov cx, koniec_Buf_1-Buf_1
	mov bx, OFFSET Buf_1 	;wpisanie do rejestru BX obszaru
	
	mov  si, 0   ;pocz�tkowa warto��wyniku konwersji w SI 
	
	wczytywanie:  mov  ah, 1   ;wczytanie znaku w kodzie ASCII 
	int  21H    ;z klawiatury do AL
	cmp  al, 13 
	je  nacis_enter  ;skok gdy naci�ni�to klawisz Enter 
	mov [bx], al
	inc bx
	
	push bx
	sub  al, 30H   ;zamaiana kodu ASCII na warto��cyfry 
	mov  bl, al   ;przechowanie kolejnej cyfry w AL 
	mov  bh, 0   ;zerowanie rejestru BH 
	mov  ax, 4   ;mno�nik 
	mul  si    ;mno�enie dotychczas uzyskanego wyniku przez 
	;10 iloczyn zostaje wpisany do rejestr�w DX:AX 
	add  ax, bx   ;dodanie aktualnie wczytanej cyfry 
	mov  si, ax   ;przes�anie wyniku obliczenia do rejestru SI 
	pop bx
	
	loop  wczytywanie
	
nacis_enter:
	
	;przejscie do nowej lini
	mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	;przejscie powrot karetki
	mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	; juz zamienione na binarke
	
	mov LBin, SI
	mov ax, LBin
	
	mov dl, al
	mov ah, 2
	int 21H
	
	mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	;przejscie powrot karetki
	mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	
	
	mov ax, LBin
	mov  cx, 4  ;liczba obieg�w p�tli 
	mov  bx, 5  ; dzielnik 
	mov  si, 3  ;indeks pocz�tkowy w tablicy tbl_cyfr 
p1:  mov  dx, 0  ;zerowanie starszej cz�ci dzielnej 
	div  bx   ;dzielenie przez 10; iloraz w AX, reszta w DX 
	add  dx, 30H  ;zamiana reszty na kod ASCII 
	mov  buf_2[si], dl ;odes�anie kodu ASCII kolejnej cyfry do tablicy 
	dec  si   ;zmniejszenie indeksu w rejestrze SI o 1 
	loop  p1   ;sterowanie p�tl�
	;usuwanie zer nieznacz�cych z lewej strony 
	mov  cx, 4  ;liczba obieg�w p�tli usuwania zer nieznacz�cych 
	mov  si, 0  ;indeks pocz�tkowy w tablicy cyfry 
p2:  cmp  byte PTR buf_2[si], 30H ;czy cyfra '0' 
	jne  druk 
	mov  byte PTR buf_2[si], 20H ;wpisanie kodu spacji wmiejsce zera 
	inc  si 
	loop p2 
;wy�wietlanie cyfr 
druk: mov  cx, 4  ;liczba obieg�w p�tli 
	mov  si, 0  ;indeks pocz�tkowy w tablicy cyfry 
p3:  mov  dl, buf_2[si] ;pobranie kodu ASCII kolejnej cyfry 
	mov  ah, 2  ;wy�wietlenie cyfry na ekranie 
	int  21H 
	inc  si   ;zwi�kszenie indeksu w rejestrze SI o 1 
	loop  p3   ;sterowanie p�tl�wy�wietlania 
	
	
	mov dl, 13 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	;przejscie powrot karetki
	mov dl, 10 	;wpisanie do rejestru DL kodu ASCII
	mov  ah, 2
	int 21H
	
	mov cx, koniec_Buf_1-Buf_1
	mov bx, OFFSET Buf_1 	;wpisanie do rejestru BX obszaru
	
	
wypis:
	mov dl, [bx]
	mov ah, 2
	int 21h
	
	inc bx
	loop wypis
	
	
	mov al, 0 	;kod powrotu programu (przekazywany przez
				;rejestr AL) stanowi syntetyczny opis programu
				;przekazywany do systemu operacyjnego
				;(zazwyczaj kod 0 oznacza, �e program zosta�
				;wykonany poprawnie)

		mov ah, 4CH 	;zako�czenie programu � przekazanie sterowania
				;do systemu, za pomoc� funkcji 4CH DOS
		int 21H
	
rozkazy ENDS

nasz_stos SEGMENT stack 	;segment stosu
dw 128 dup (?)
nasz_stos ENDS

END wystartuj 			;wykonanie programu zacznie si� od rozkazu
				;opatrzonego etykiet� wystartuj