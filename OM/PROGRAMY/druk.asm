dane        SEGMENT ;segment danych
Buf_1		db 4 dup(0)
LBin		dw 0 
Buf_2		db 128 dup(0)
nlcr        db 0dh, 0ah, "$"
dane        ENDS

rozkazy     SEGMENT ;segment rozkazu
            ASSUME cs:rozkazy, ds:dane
    startuj:    mov bx, SEG dane
                mov ds, bx
				
				mov si, 0 
				mov cx, 5
				mov di, offset Buf_1
	czytaj:		MOV AH,08    ; Function to read a char from keyboard
				INT 21h      ; the char saved in AL
				;sub al, 30H  ;zamaiana kodu ASCII na wartość cyfry
				cmp al, 13
				je skok ;skok gdy naciśnięto klawisz Enter
				MOV AH,02    ; Function to display a char  
				MOV AH,02    ; Function to display a char  
				;sub al, 30H
				MOV DL,AL    ; Copy a saved char in AL to DL to output it
				INT 21h
				
				MOV [di],DL    ; Copy a saved char in AL to DL to output it
				inc di
				
				sub al, 30H
				mov bl, al ;przechowanie kolejnej cyfry w AL
				mov bh, 0 ;zerowanie rejestru BH
				mov ax, 5 ;mnożnik
				mul si ;mnożenie dotychczas uzyskanego wyniku przez
				;10 iloczyn zostaje wpisany do rejestrów DX:AX
				add ax, bx ;dodanie aktualnie wczytanej cyfry
				mov si, ax
				loop czytaj 
				
	skok:		mov di, offset LBin
				mov[di], si
				mov cx, 0 ;licznik cyfr
				mov bx, 7 ;dzielnik
				mov ax, si
	p1: 		mov dx, 0 ;zerowanie starszej części dzielnej
				div bx ;dzielenie przez 10 – iloraz w AX, reszta w DX
				add dx, 30H ;zamiana reszty na kod ASCII
				push dx ;zapisanie cyfry na stosie
				inc cx ;inkrementacja licznika cyfr
				cmp ax, 0 ;porównanie uzyskanego ilorazu
				jnz p1 ;skok gdy iloraz jest różny od zera 
				
				
				mov di, offset Buf_2
	p2: 		pop dx ;pobranie kodu ASCII kolejnej cyfry
				mov[di], dx
				inc di
				loop p2 ; sterowanie pętlą wyświetlania 
				
				
				
	wypisz:		
				
				mov dx,offset nlcr
                mov ah, 09h
                int 21h
				
				
				
				mov dx,offset nlcr
                mov ah, 09h
                int 21h
				;mov dx, offset Buf_1
                ;mov ah, 09h
                ;int 21h
				
				
				
				
				;mov dx, offset LBin
                ;mov ah, 09h
                ;int 21h
				
				mov dx, offset Buf_2
                mov ah, 09h
                int 21h
				
				
				

				mov al, 0
                mov ah, 4CH
                int 21H
rozkazy ENDS

stosik SEGMENT stack
    dw      128 dup(?)
stosik ENDS

END     startuj 